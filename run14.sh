#!/bin/bash

Sn=14
Sn2=14
Length=80

ulimit -Sn 100000
ulimit -n 100000

G5K=true
if [ G5K=true ]
then
    echo "Computing on grid5000 !"
else
    echo "Computing on a single computer !"
fi

INITIAL_WORKING_DIRECTORY=$(pwd)
TIMESTAMP=$(date +%s)

echo

SUFFIX="_${Sn}_${Sn2}_${Length}" #$(eval date +"%Y-%m-%d")_"$TIMESTAMP"

SOURCEDIR="$INITIAL_WORKING_DIRECTORY/sources"
echo "sources : $SOURCEDIR"

BINDIR="$INITIAL_WORKING_DIRECTORY/bin${SUFFIX}"
echo "bin : $BINDIR"
rm -rf "$BINDIR"
mkdir -p "$BINDIR"

INITFILENAME=$BINDIR/"init"
rm -rf "$EXECFILENAME"
echo "executable file : $EXECFILENAME"

EXECFILENAME=$BINDIR/"StollProblem"
rm -rf "$EXECFILENAME"
echo "executable file : $EXECFILENAME"

DATADIR="$INITIAL_WORKING_DIRECTORY/data${SUFFIX}"
rm -rf "$DATADIR"
mkdir -p "$DATADIR"
echo "data directory : $DATADIR"

ARGSFILE=$DATADIR/"arguments${SUFFIX}.ini"
rm -rf "$ARGSFILE"
echo "args file : $ARGSFILE"
#touch "$ARGSFILE"

LOGDIR="$INITIAL_WORKING_DIRECTORY/log${SUFFIX}"
rm -rf "$LOGDIR"
mkdir -p "$LOGDIR"
echo "log directory : $LOGDIR"

FINALLOGFILE=$LOGDIR/"log${SUFFIX}.log"
rm -rf "$FINALLOGFILE"
echo "final log file : $FINALLOGFILE"

if [ "$G5K" = true ]
then
    NODEFILE=$DATADIR/"nodes${SUFFIX}"
    rm -rf "$NODEFILE"
    nb_available_cores=0
    for i in $(uniq $OAR_NODEFILE);
    do
	tmp=$(expr $(oarsh $i grep -c ^processor /proc/cpuinfo) / 2)
	echo $tmp/$i >> "$NODEFILE"
	nb_available_cores=$(expr $nb_available_cores + $tmp)
    done
fi

echo "$nb_available_cores available"

cd "$SOURCEDIR"
make init BINDIR="$BINDIR" --always-make
cd "$INITIAL_WORKING_DIRECTORY"
"$INITFILENAME" $Sn $Sn2 $Length $DATADIR $ARGSFILE

echo "Compilation..."
cd "$SOURCEDIR"
make EXECFILE="$EXECFILENAME" 
cd "$INITIAL_WORKING_DIRECTORY"
echo "Compilation terminée..."

echo "Calcul des entiers..."

start=$(date +"%D at %T")
echo "Start : $start" >> $FINALLOGFILE

if [ "$G5K" = true ]
then
    parallel --ssh $(which oarsh) --sshloginfile "$NODEFILE" --colsep ' ' --arg-file "$ARGSFILE" "$EXECFILENAME"
else
    parallel --colsep ' ' --arg-file "$ARGSFILE" "$EXECFILENAME"
fi
echo "Calcul des entiers terminé..."

end=$(date +"%D at %T")
echo "End : $end" >> "$FINALLOGFILE"

exit


