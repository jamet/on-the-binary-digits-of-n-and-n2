/*****
Compute all the odd integers whose sum of digits and equal to the sum of the digits of its square
The sum of the digits and the max size are fixed

Authors : D. Jamet, P. Popoli, T. Stoll
Date : 2022-02-28
*****/

#include <iostream>
#include <stdint.h>
#include "libpopcnt.h"

#include <iomanip> // setw
#include <unistd.h> // getpid
#include <sstream> // stringstream
#include <fstream> // ofstream

typedef uint64_t uint64  ;
typedef uint64_t index_t ;
typedef uint64_t length_t  ;
typedef uint64_t weight_t ;
typedef uint64_t counter_t  ;

typedef unsigned __int128 uint128;

#define NUMBER_OF_SET_BITS 4

std::ostream& operator<<(std::ostream&, const uint128&);
uint64 binomial(uint64 n , uint64 k ); // n!/k!/(n-k)!

void next(uint64*); // Given an integer n0, compute the smallest integer n1, strictly greater than n0, and of the same weight as n0
void next(uint128*); // Similar to next(uint64*), except the argument type is uint128*

void mult64to128(uint64 x, uint64 y, uint64& hi, uint64& lo); //Computes the product of two 64-bit unsigned integers and stores the high part of the product in hi and the low part in lo

bool sqr64to128(uint64 x, weight_t  SUM_OF_DIGITS_OF_N2);  // Computes the product of a 64-bit unsigned integer x : returns true if and only if the sum of the digits of x^2 is equal to SUM_OF_DIGITS_OF_N2 
bool sqr128to256(uint128, weight_t); // Similar to sqr64to128, except the argument type is uint128

int ffsll_u128(uint128* x); // Returns one plus the index of the least significant 1-bit of x, or if x is zero, returns zero. 

void Searching4Integers( weight_t SUM_OF_DIGITS_OF_N,
			 weight_t SUM_OF_DIGITS_OF_N2,
			 index_t a, // position of the 2nd rightmost set bit of n
			 index_t b, // position of the 3rd rightmost set bit of n
			 index_t c, // position of the 24th rightmost set bit of n
			 index_t L, // max binary length of n
			 std::string PATHNAME); // pathname (directory) where to store log and data files
/* 
OUTPUT_FILENAME = PATHNAME/<SUM_OF_DIGITS_OF_N>_<SUM_OF_DIGITS_OF_N2>_<a>_<b>_<c>_<L>.out
LOG_FILENAME = PATHNAME/<SUM_OF_DIGITS_OF_N>_<SUM_OF_DIGITS_OF_N2>_<a>_<b>_<c>_<L>.log
Calls step64(SUM_OF_DIGITS_OF_N, SUM_OF_DIGITS_OF_N2, a, b, c, L, found, tested, OUTPUT_FILENAME, LOG_FILENAME);
The variable 'found' receives the number of found integers
The variable 'tested' receives the number of tested integers
*/ 

void step64(counter_t SUM_OF_DIGITS_OF_N,
	    counter_t SUM_OF_DIGITS_OF_N2,
	    index_t a, index_t b, index_t c, length_t L, // 0 < a < b < c 
	    counter_t* found, counter_t* tested,
	    std::string OUTPUT_FILENAME,
	    std::string LOG_FILENAME);
/* 
Search all the odd integers of weight SUM_OF_DIGITS_OF_N :
  whose square is of weight SUM_OF_DIGITS_OF_N2.
  whose the a-th bit, the b-th bit, the c-th bit are set to 1 
  whose binary length is at most L, 
The results and logs files are stored in OUTPUT_FILENAME and LOG_FILENAME.
*/

void step128(weight_t,  weight_t, index_t, index_t, index_t, length_t, counter_t*, counter_t*, std::string, std::string);
// Similar to step64 for integers of length from 65 bits to 128 bits 

std::ostream& operator<<(std::ostream& o, const uint128& x) {
  if (x < 10)
    return o << (char)(x + '0');
  return o << (x / 10) << (char)(x % 10 + '0');
}

uint64 binomial(uint64 N, uint64 K){
  if (N < K)
    return 0;
  uint64 C[K+1];
  C[0] = 1;
  for (index_t k = 0; k < K; ++ k)
    C[k+1] = (C[k] * (N-k)) / (k+1);
  return C[K];
}

/*********************************************************************************************************/
/***                                     up to 64 bits !!!!!                                           ***/
/*********************************************************************************************************/

void next(uint64* a){ //, uint64* len){ // len_0 = 10
  counter_t b,c;
  c = __builtin_ffsll(*a) - 1;
  *a = (*a >> c);
  *a += 1;
  b = __builtin_ffsll(*a) - 1;
  b -= 1;
  *a = *a << c;
  *a = *a | (((uint64)1)<<b);
  *a -= 1;
}

void mult64to128(uint64 x, uint64 y, uint64& hi, uint64& lo){
  __asm__(
	  "mulq %3\n\t"
	  : "=d" (hi),
	    "=a" (lo)
	  : "%a" (x),
	    "rm" (y)
	  : "cc" );
}

bool sqr64to128(uint64 x, weight_t  SUM_OF_DIGITS_OF_N2){
  uint64 Hi, Lo;
  mult64to128(x, x, Hi, Lo);
  uint64 popLo;
  popLo = popcnt(&Lo, sizeof(uint64));
  
  if (popLo > SUM_OF_DIGITS_OF_N2)
    return false;

  uint64 popHi;
  popHi = popcnt(&Hi, sizeof(uint64));

  return popLo + popHi == SUM_OF_DIGITS_OF_N2;
}

/*********************************************************************************************************/
/***                                     up to 128 bits !!!!!                                          ***/
/*********************************************************************************************************/

int ffsll_u128(uint128* u){
  uint64 lo = *u;
  if (lo != 0)
    return __builtin_ffsll(lo);
  uint64 hi = (*u) >> 64;
  return __builtin_ffsll(hi)+64;
}

void next(uint128* a){ //, uint64* len){ // len_0 = 10
  counter_t b,c;
  c = ffsll_u128(a)-1;
  *a = (*a >> c);
  *a += 1;
  b = ffsll_u128(a)-1;
  b -= 1;
  *a = *a << c;
  *a = *a | (((uint128)1)<<b);
  *a -= 1;
}

bool sqr128to256(uint128 r, weight_t SUM_OF_DIGITS_OF_N2) 
{
  uint128 r1 = (r & 0xffffffffffffffff); // low part of r
  uint128 t = (r1 * r1);
  uint128 w3 = (t & 0xffffffffffffffff); // low part of t
  uint128 k = (t >> 64);
  
  r >>= 64;
  uint128 m = (r * r1);
  t = m + k;
  uint128 w2 = (t & 0xffffffffffffffff); // low part of t
  uint128 w1 = (t >> 64);
  
  t = m + w2;

  uint128 lo = (t << 64) + w3;
  
  const counter_t pop_low = popcnt(&lo, sizeof(uint128));
  if (pop_low > SUM_OF_DIGITS_OF_N2)
    return false;

  k = (t >> 64);
  uint128 hi = (r * r) + w1 + k;
  const counter_t pop_hi = popcnt(&hi, sizeof(uint128));

  return (pop_low+pop_hi == SUM_OF_DIGITS_OF_N2);
}

void step64(counter_t SUM_OF_DIGITS_OF_N,
	    counter_t SUM_OF_DIGITS_OF_N2,
	    index_t a, index_t b, index_t c, length_t L, // 0 < a < b < c 
	    counter_t* found, counter_t* tested,
	    std::string OUTPUT_FILENAME,
	    std::string LOG_FILENAME){
  if (L + NUMBER_OF_SET_BITS >= SUM_OF_DIGITS_OF_N + c + 1){
    const uint64 ONE = 0x1;
    const uint64 suffix = (ONE << c) + (ONE << b) + (ONE << a) + 1 ;

    const uint64 LAST_INTEGER =  ((( ONE << (SUM_OF_DIGITS_OF_N-NUMBER_OF_SET_BITS))-1) << (L-(SUM_OF_DIGITS_OF_N-NUMBER_OF_SET_BITS))) + suffix;
    
    std::ofstream outputfile;
    outputfile.open(OUTPUT_FILENAME, std::ofstream::out); // erase file if it exists
    outputfile.close();
    
    counter_t tmp_tested = 0;
    counter_t tmp_found = 0;
    
    uint64 NUMBER_OF_INTEGERS_TO_TEST = binomial(L - c - 1, SUM_OF_DIGITS_OF_N-NUMBER_OF_SET_BITS);
    
    uint64 z = ((uint64)1 << (SUM_OF_DIGITS_OF_N-NUMBER_OF_SET_BITS))-1;    
    uint64 Z = ((uint64)z << (c+1)) | suffix; 

    uint128 LAST_TESTED = Z;
    std::cout << "Started at = " << Z << std::endl;
    /************************************** Updating the log file **************************************************/
    time_t t = time(NULL);
    struct tm start;
    start = *localtime(&t);
    
    std::ofstream logfile;
    logfile.open(LOG_FILENAME, std::ofstream::app);
    
    logfile << "\nUp to " << L << " bits..." << std::endl;
    logfile << "Computation started at "
	    << start.tm_year + 1900 << "-"
	    << std::setw(2) << std::setfill('0') << start.tm_mon + 1 << "-"
	    << std::setw(2) << std::setfill('0') << start.tm_mday << " "
	    << std::setw(2) << std::setfill('0') << start.tm_hour << ":"
	    << std::setw(2) << std::setfill('0') << start.tm_min << "-"
	    << std::setw(2) << std::setfill('0') << start.tm_sec << std::endl;
    
    logfile << "FIRST INTEGER TO TEST :\t\t" << Z << std::endl;
    logfile << "LAST INTEGER TO TEST : \t\t" << LAST_INTEGER << std::endl;
    logfile << "NUMBER OF INTEGERS TO TEST :\t" << NUMBER_OF_INTEGERS_TO_TEST << std::endl;
    logfile.close();
    /**************************************************************************************************************/
  
    while(tmp_tested != NUMBER_OF_INTEGERS_TO_TEST){
      LAST_TESTED = Z;
      ++(tmp_tested);
      if (sqr64to128(Z, SUM_OF_DIGITS_OF_N2)){
	++(tmp_found);
	
	outputfile.open(OUTPUT_FILENAME, std::ofstream::app);
	outputfile << Z << std::endl;
	outputfile.close();
	
      }
      next(&z);
      Z = (z << (c+1)) | suffix;
    }
    *tested += tmp_tested;
    *found += tmp_found;

    /************************************** Updating the log file *************************************************/
    struct tm end;
    t = time(NULL);
    end = *localtime(&t);
    
    logfile.open(LOG_FILENAME, std::ofstream::app);
    
    logfile << "Computation ended at "
	    << end.tm_year + 1900 << "-"
	    << std::setw(2) << std::setfill('0') << end.tm_mon + 1 << "-"
	    << std::setw(2) << std::setfill('0') << end.tm_mday << " "
	    << std::setw(2) << std::setfill('0') << end.tm_hour << ":"
	    << std::setw(2) << std::setfill('0') << end.tm_min << "-"
	    << std::setw(2) << std::setfill('0') << end.tm_sec << std::endl;
    
    logfile << "\tLAST TESTED INTEGER : \t"  << LAST_TESTED << std::endl;
    logfile << "\t#TESTED : \t\t"  << tmp_tested << std::endl;
    logfile.close();
    /**************************************************************************************************************/
  
  }
}
  
void step128(weight_t SUM_OF_DIGITS_OF_N,
	     weight_t SUM_OF_DIGITS_OF_N2,
	     index_t a, index_t b, index_t c, length_t L,
	     counter_t* found, counter_t* tested,
	     std::string OUTPUT_FILENAME,
	     std::string LOG_FILENAME){
  
  const uint128 ONE = 0x1;
  const uint128 suffix = (ONE << c) + (ONE << b) + (ONE << a) + 1 ;
  const uint128 LAST_INTEGER =  ((( ONE << (SUM_OF_DIGITS_OF_N - NUMBER_OF_SET_BITS))-1) << (L-(SUM_OF_DIGITS_OF_N-NUMBER_OF_SET_BITS))) + suffix;
  
  std::ofstream outputfile;
  outputfile.open(OUTPUT_FILENAME, std::ofstream::app); // do not erase file if it exists
  outputfile.close();
  
  counter_t tmp_tested = 0;
  counter_t tmp_found = 0;
  
  uint128 NUMBER_OF_INTEGERS_TO_TEST;
  uint128 z, Z;

  uint128 LASTINTEGER64;
  if (64 + NUMBER_OF_SET_BITS >= c+1 + SUM_OF_DIGITS_OF_N){
    NUMBER_OF_INTEGERS_TO_TEST = binomial(L - c - 1, SUM_OF_DIGITS_OF_N-NUMBER_OF_SET_BITS) - binomial(64 - c - 1, SUM_OF_DIGITS_OF_N-NUMBER_OF_SET_BITS);
    LASTINTEGER64 = ((( ONE << (SUM_OF_DIGITS_OF_N-NUMBER_OF_SET_BITS))-1) << (64-(SUM_OF_DIGITS_OF_N-NUMBER_OF_SET_BITS))) + suffix;
    z = LASTINTEGER64 >> (c+1);
    next(&z);
  }
  else{
    NUMBER_OF_INTEGERS_TO_TEST = binomial(L - c - 1, SUM_OF_DIGITS_OF_N-NUMBER_OF_SET_BITS);
     z = (ONE << (SUM_OF_DIGITS_OF_N-NUMBER_OF_SET_BITS))-1;
  }

  Z = (z << (c+1)) | suffix; 
  std::cout << "Started at = " << Z << std::endl;
  
  uint128 LAST_TESTED = Z;
  
  /************************************** Updating the log file **************************************************/
  time_t t = time(NULL);
  struct tm start;
  start = *localtime(&t);
  
  std::ofstream logfile;
  logfile.open(LOG_FILENAME, std::ofstream::app);
  
  logfile << "\nUp to " << L << " bits..." << std::endl;
  logfile << "Computation started at "
	  << start.tm_year + 1900 << "-"
	  << std::setw(2) << std::setfill('0') << start.tm_mon + 1 << "-"
	  << std::setw(2) << std::setfill('0') << start.tm_mday << " "
	  << std::setw(2) << std::setfill('0') << start.tm_hour << ":"
	  << std::setw(2) << std::setfill('0') << start.tm_min << "-"
	  << std::setw(2) << std::setfill('0') << start.tm_sec << std::endl;
  
  logfile << "FIRST INTEGER TO TEST :\t\t" << Z << std::endl;
  logfile << "LAST INTEGER TO TEST : \t\t" << LAST_INTEGER << std::endl;
  logfile << "NUMBER OF INTEGERS TO TEST :\t" << NUMBER_OF_INTEGERS_TO_TEST << std::endl;
  logfile.close();
  /**************************************************************************************************************/
  
  while(tmp_tested != NUMBER_OF_INTEGERS_TO_TEST){
    LAST_TESTED = Z;
    ++(tmp_tested);
    if (sqr128to256(Z, SUM_OF_DIGITS_OF_N2)){
      ++(tmp_found);
      outputfile.open(OUTPUT_FILENAME, std::ofstream::app);
      outputfile << Z << std::endl;
      outputfile.close();
    }
    next(&z);
    Z = (z << (c+1)) | suffix; 
  }
  *tested += tmp_tested;
  *found += tmp_found;
  
  /************************************** Updating the log file *************************************************/
  struct tm end;
  t = time(NULL);
  end = *localtime(&t);
  
  logfile.open(LOG_FILENAME, std::ofstream::app);
  
  logfile << "Computation ended at "
	  << end.tm_year + 1900 << "-"
	  << std::setw(2) << std::setfill('0') << end.tm_mon + 1 << "-"
	  << std::setw(2) << std::setfill('0') << end.tm_mday << " "
	  << std::setw(2) << std::setfill('0') << end.tm_hour << ":"
	  << std::setw(2) << std::setfill('0') << end.tm_min << "-"
	  << std::setw(2) << std::setfill('0') << end.tm_sec << std::endl;
  
  logfile << "\tLAST TESTED INTEGER : \t"  << LAST_TESTED << std::endl;
  logfile << "\t#TESTED : \t\t"  << tmp_tested << std::endl;
  logfile.close();
  /**************************************************************************************************************/
}

void Searching4Integers(
		    weight_t SUM_OF_DIGITS_OF_N,
		    weight_t SUM_OF_DIGITS_OF_N2,
		    index_t a,
		    index_t b,
		    index_t c,
		    index_t L,
		    std::string PATHNAME){
  
  std::stringstream BASENAME_STREAM;
  std::string LOG_FILENAME, OUTPUT_FILENAME, PID_FILENAME;

  BASENAME_STREAM << std::setw(3) << std::setfill('0') << SUM_OF_DIGITS_OF_N << "_"
		  << std::setw(3) << std::setfill('0') << SUM_OF_DIGITS_OF_N2 << "_"
		  << std::setw(3) << std::setfill('0') << a << "_"
		  << std::setw(3) << std::setfill('0') << b << "_"
		  << std::setw(3) << std::setfill('0') << c << "_"
		  << std::setw(3) << std::setfill('0') << L;

  LOG_FILENAME =  PATHNAME + "/" + BASENAME_STREAM.str() + ".log";
  OUTPUT_FILENAME = PATHNAME + "/" + BASENAME_STREAM.str() + ".out";
  PID_FILENAME = PATHNAME + "/" + BASENAME_STREAM.str() + ".pid";
  
  /************************************** The pid file **************************************************/
  const pid_t pid = getpid();
  std::ofstream pidfile;
  pidfile.open(PID_FILENAME, std::ofstream::out);
  pidfile << pid << std::endl;
  pidfile.close();
  /*****************************************************************************************************/

  
  /************************************** The log file **************************************************/
  time_t t = time(NULL);
  struct tm start, end;
  start = *localtime(&t);
  
  std::ofstream logfile, outputfile;
  logfile.open(LOG_FILENAME, std::ofstream::out);

  char cmd[100];
  sprintf(cmd, "\nhostname >> %s", LOG_FILENAME.c_str());
  system(cmd);
  
  logfile << "Computation started at "
	  << start.tm_year + 1900 << "-"
	  << std::setw(2) << std::setfill('0') << start.tm_mon + 1 << "-"
    	  << std::setw(2) << std::setfill('0') << start.tm_mday << " "
    	  << std::setw(2) << std::setfill('0') << start.tm_hour << ":"
    	  << std::setw(2) << std::setfill('0') << start.tm_min << "-"
    	  << std::setw(2) << std::setfill('0') << start.tm_sec << std::endl;
  logfile.close();
  /*****************************************************************************************************/

  
  /*****************************************************************************************************/
  /*********************************** looking for integers  *******************************************/
  /*****************************************************************************************************/
  counter_t found, tested;

  found = 0;
  tested = 0;

  if (L <= 64){
    std::cout << "Up to " << L << " bits...\n" ;
    step64(SUM_OF_DIGITS_OF_N,  SUM_OF_DIGITS_OF_N2, a, b, c, L, &found, &tested, OUTPUT_FILENAME, LOG_FILENAME);
  }
  else
    {
      std::cout << "Up to 64 bits...\n" ;
      step64(SUM_OF_DIGITS_OF_N,  SUM_OF_DIGITS_OF_N2, a, b, c, 64, &found, &tested, OUTPUT_FILENAME, LOG_FILENAME);
      if (L <= 128){
	std::cout << "Up to " << L << " bits...\n" ;
	step128(SUM_OF_DIGITS_OF_N,  SUM_OF_DIGITS_OF_N2, a, b, c, L, &found, &tested, OUTPUT_FILENAME, LOG_FILENAME);
      }
      else{
	std::cout << "Up to 128 bits...\n" ;
	step128(SUM_OF_DIGITS_OF_N,  SUM_OF_DIGITS_OF_N2, a, b, c, 128, &found, &tested, OUTPUT_FILENAME, LOG_FILENAME);
      }
    }
  
  std::cout << "\n\n" << found << " found among " << tested << " tested" << std::endl;

  /*****************************************************************************************************/
  /*****************************************************************************************************/
  /*****************************************************************************************************/
  
  t = time(NULL);
  end = *localtime(&t);

  logfile.open(LOG_FILENAME, std::ofstream::app);
  logfile << "\n\n" << found << " found among " << tested << " tested" << std::endl;

  logfile << "Computation ended at "
	  << start.tm_year + 1900 << "-"
	  << std::setw(2) << std::setfill('0') << end.tm_mon + 1 << "-"
	  << std::setw(2) << std::setfill('0') << end.tm_mday << " "
	  << std::setw(2) << std::setfill('0') << end.tm_hour << ":"
	  << std::setw(2) << std::setfill('0') << end.tm_min << "-"
	  << std::setw(2) << std::setfill('0') << end.tm_sec << std::endl;
  logfile.close();
}


int main(int argc, char** argv){
  std::string filename = argv[1];
  std::ifstream infile(filename);

  weight_t SN, SN2;
  index_t X, Y, Z;
  length_t L;
  std::string path;
  while (infile >> SN >> SN2 >> X >> Y >> Z >> L >> path){
    Searching4Integers(SN, SN2, X, Y, Z, L, path);
  }

  return EXIT_SUCCESS;

}

