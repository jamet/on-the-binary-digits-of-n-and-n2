#include <iostream>     // std::cout, std::ostream, std::ios
#include <fstream>      // std::filebuf
#include <iomanip>      // setw
#include <sstream>      // std::stringstream
#include <vector>
#include <set>

typedef unsigned long long int ull;
typedef unsigned long long int position_t;
typedef unsigned long long int weight_t;
typedef unsigned long long int length_t;
typedef std::string pathname_t;

ull binomial(ull N, ull K){
  ull C[K+1];
  C[0] = 1;
    for (ull k = 0; k < K; ++ k)
      C[k+1] = (C[k] * (N-k)) / (k+1);
  return C[K];
}

typedef struct argument{
  weight_t Sn; // weight of n
  weight_t Sn2; // weight max of n^2
  
  position_t x ; // position of the second rightmost set bit
  position_t y ; // position of the third rightmost set bit
  position_t z ; // position of the fourth rightmost set bit

  length_t L; // max length of n

  pathname_t pathname; // directory

  bool selected;  
} argument_t;

std::ostream& operator<< (std::ostream& os, const argument_t& A){
  os << A.Sn << " "
     << A.Sn2 << " "
     << A.x << " "
     << A.y << " "
     << A.z << " "
     << A.L << " "
     << A.pathname ;
    return os;
}

ull value(argument A){
  return binomial(A.L-A.z-1, A.Sn-4);
}

argument_t new_argument(weight_t Sn,
			weight_t Sn2,
			position_t x,
			position_t y,
			position_t z,
			length_t L,
			pathname_t pathname){
  argument_t Arg;
  Arg.Sn = Sn;
  Arg.Sn2 = Sn2;
  Arg.x = x;
  Arg.y = y;
  Arg.z = z;
  Arg.L = L;
  Arg.pathname = pathname;
  Arg.selected = false;
  return Arg;
}

int main(int argc, char** argv)
{
  weight_t Sn = atoi(argv[1]);
  weight_t Sn2 = atoi(argv[2]);
  length_t L = atoi(argv[3]);
  std::string pathname = argv[4];
  std::string arg_filename = argv[5];

  ull Zmax = L-Sn+3;
  ull NumberOfArgumentFiles = binomial(Zmax,3);
  std::cout << "# of argument files : " << NumberOfArgumentFiles << std::endl;

  /* on construit l'ensemble des configurations/arguments */
  std::vector<argument_t> ARGS;
  for(position_t z = 3 ; z < Zmax+1 ; ++z)
    for(position_t x = 1 ; x < z-1 ; ++x)
      for(position_t y = x+1 ; y < z ; ++y){
	ARGS.push_back(new_argument(Sn, Sn2, x, y, z, L, pathname));
      }
  /* fin construction des configurations/arguments */

  ull remain = NumberOfArgumentFiles;
    
  std::cout << std::endl << std::endl;

  std::vector<argument> *Cores = new std::vector<argument>[100000]; // 4000 par exemple
  
  ull idx_core = 0;
  Cores[idx_core].push_back(ARGS[0]);
  remain -= 1;

  ull MAX = value(Cores[0][0]);
  
  ull S, j;
  std::stringstream ss_core;
  std::stringstream ss_arg;
  std::ofstream myfile;

  while(remain > 0){
    S = 0;
    j = idx_core;

    ss_core << pathname << "/" << 
      std::setfill('0') << std::setw(2) << Sn << "_" <<
      std::setfill('0') << std::setw(2) << Sn2 << "_" <<
      std::setfill('0') << std::setw(3) << L <<
      "_core_" << 
      std::setfill('0') << std::setw(4) << idx_core << ".arg"; 

    ss_arg << ss_core.str() << std::endl;
    
    myfile.open(ss_core.str());
    
    while ((remain > 0) && (j < NumberOfArgumentFiles)){
      if ((ARGS[j].selected == false) && (S + value(ARGS[j]) <= MAX)){
	S += value(ARGS[j]);
	ARGS[j].selected = true;
	myfile << ARGS[j] << std::endl;
	
	Cores[idx_core].push_back(ARGS[j]); // <---- ici on peut créer le fichier
	remain -= 1;
      }
      j += 1;
    }
    myfile.close();
    // ici on écrit le fichier correspondant au core      
    ss_core.str("");
    idx_core += 1;    
}
  
  myfile.open(arg_filename);
  myfile << ss_arg.str();
  myfile.close();
  std::cout << "Nombre de cores utiles : " << idx_core << std::endl;
  return EXIT_SUCCESS;
}


