#include <iostream>
#include <gmpxx.h>

#include <stack>
#include <queue>

#define SN2 4

typedef uint64_t uint64  ;
typedef uint64_t length_t;
typedef uint64_t weight_t;
typedef uint64_t count_t;

typedef mpz_class BigInteger;

struct MetaInteger_t{
  BigInteger value;
  weight_t weight;
  length_t length;
  length_t leadingZeros;
};

mp_bitcnt_t popcount(BigInteger* N);
bool add_1_at_left(MetaInteger_t* N, MetaInteger_t* out);
bool add_0_at_left(MetaInteger_t* N, MetaInteger_t *out);
bool IsCandidate(MetaInteger_t* N);

void JaPoSt();
void JaPoSt(weight_t, weight_t);

std::ostream& operator<<(std::ostream& o, const MetaInteger_t& x) {
  o << x.value;
  return o;
}

mp_bitcnt_t popcount(BigInteger* N){
  return mpz_popcount(N->get_mpz_t());
}

bool add_1_at_left(MetaInteger_t* N, MetaInteger_t* out, weight_t* SN_MAX){
  if (N->weight < *SN_MAX){
    *out = *N;
    out->value |= (1 << (N->length));
    out->length++;
    out->weight++;
    out->leadingZeros = 0;
    return true;
  }
  return false;
}

bool add_0_at_left(MetaInteger_t* N, MetaInteger_t *out){
  if (2*N->leadingZeros <= N->length-1){
    *out = *N;
    out->length++;
    out->leadingZeros++;
    return true;
  }
  return false;
}

bool IsCandidate(MetaInteger_t* M){
  BigInteger sum = M->value + (M->value)*(M->value) ; 
  sum &= sum & ((1 << (M->length)) - 1);
  return (popcount(&sum) < (SN2-1));
}

void JaPoSt(weight_t SN_MAX){
  std::stack<MetaInteger_t> S;
  MetaInteger_t M_0 = {1, 1, 1, 0}; // N_0 : first value to test 
  MetaInteger_t M;
  MetaInteger_t tmp;
  BigInteger n, n2;
  
  S.push(M_0); // initialisation de la pile
  
  count_t stacked = 1;
  length_t MaxLength = 1;
  length_t StackHeight = 1;
  length_t MaxStackHeight = 1;
  
  while (!S.empty()){

    n = 1+2*M.value; // n = 1+2^l*m, ici l = 1

    n2 = n*n;
    
    if (popcount(&n2) == SN2)
      std::cout << n << std::endl << std::flush;
 
    M = S.top();
    S.pop(); StackHeight--;
    if ((add_1_at_left(&M, &tmp, &SN_MAX)) && (IsCandidate(&tmp))){
      S.push(tmp);
      stacked++;

      StackHeight++;
      if (MaxStackHeight < StackHeight)
	MaxStackHeight = StackHeight;

      if (MaxLength < tmp.length)
	MaxLength = tmp.length;
    }
    if ((add_0_at_left(&M, &tmp) && (IsCandidate(&tmp)))){
      S.push(tmp);
      stacked++;

      StackHeight++;
      if (MaxStackHeight < StackHeight)
	MaxStackHeight = StackHeight;

      if (MaxLength < tmp.length)
	MaxLength = tmp.length;
    } 
  }
  std::cout << "#Stacked : " << stacked << std::endl;
  std::cout << "#MaxLength : " << MaxLength << std::endl;
  std::cout << "#MaxStackHeight : " << MaxStackHeight << std::endl;  
}

int main(int argc, char** argv) {
  weight_t SN_MAX = atoi(argv[1]);
  //weight_t SN2 = atoi(argv[2]);
 
  JaPoSt(SN_MAX);
  return EXIT_SUCCESS;
}









